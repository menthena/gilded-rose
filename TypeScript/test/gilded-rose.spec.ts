import { expect } from 'chai';
import { Item, GildedRose, ItemName } from '../app/gilded-rose';

describe('Gilded Rose', () => {

    it('Adds empty array when not passed', () => {
        const gildedRose = new GildedRose();
        expect(gildedRose.items.length).to.equal(0);
    });
    it('Decreases the sellIn each time updates the quality', () => {
        const gildedRose = new GildedRose([new Item('item', 10, 10), new Item('item', 5, 2)]);
        const item = gildedRose.items[0];
        expect(item.sellIn).to.equal(10);
        gildedRose.updateQuality();
        expect(item.sellIn).to.equal(9);
        gildedRose.updateQuality();
        expect(item.sellIn).to.equal(8);
    });
    it('The quality degrades by one before the expiry', () => {
        const gildedRose = new GildedRose([new Item('item', 10, 10)]);
        const item = gildedRose.items[0];
        expect(item.quality).to.equal(10);
        gildedRose.updateQuality();
        expect(item.quality).to.equal(9);
        gildedRose.updateQuality();
        expect(item.quality).to.equal(8);
    });
    it('The Quality of an item is never negative', () => {
        const gildedRose = new GildedRose([new Item('item', 0, 0)]);
        const item = gildedRose.items[0];
        expect(item.quality).to.equal(0);
        gildedRose.updateQuality();
        expect(item.quality).to.equal(0);
    });
    it('Once the sell by date has passed, Quality degrades twice as fast', () => {
        const gildedRose = new GildedRose([new Item('item', -5, 10)]);
        const item = gildedRose.items[0];
        expect(item.quality).to.equal(10);
        gildedRose.updateQuality();
        expect(item.quality).to.equal(8);
        gildedRose.updateQuality();
        expect(item.quality).to.equal(6);
    });
    it('UpdateQuality returns the items', () => {
        const items = [new Item(ItemName.Sulfuras, 10, 10)];
        const gildedRose = new GildedRose(items);
        expect(gildedRose.updateQuality()).to.deep.equal(items);
    });
    describe(ItemName.AgedBrie, () => {
        it(`${ItemName.AgedBrie} actually increases in Quality the older it gets`, () => {
            const gildedRose = new GildedRose([new Item(ItemName.AgedBrie, 10, 0)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(0);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(1);
        });
        it('The Quality of an item is never more than 50', () => {
            const gildedRose = new GildedRose([new Item(ItemName.AgedBrie, 10, 50)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(50);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(50);
        });
        it('Once the sell by date has passed, Quality increases twice as fast', () => {
            const gildedRose = new GildedRose([new Item(ItemName.AgedBrie, -1, 5)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(5);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(7);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(9);
        });
        it('Never exceeds 50', () => {
            const gildedRose = new GildedRose([new Item(ItemName.AgedBrie, -1, 50)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(50);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(50);
        });
    });
    describe(ItemName.Sulfuras, () => {
        it(`${ItemName.Sulfuras}, being a legendary item, never has to be sold`, () => {
            const gildedRose = new GildedRose([new Item(ItemName.Sulfuras, 10, 80)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(80);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(80);
        });
        it(`${ItemName.Sulfuras}, being a legendary item never decreases in Quality`, () => {
            const gildedRose = new GildedRose([new Item(ItemName.Sulfuras, -1, 80)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(80);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(80);
        });
    });
    describe(ItemName.BackstagePasses, () => {
        it('Increases in Quality as its SellIn value approaches', () => {
            const gildedRose = new GildedRose([new Item(ItemName.BackstagePasses, 11, 1)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(1);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(2);
        });
        it('Quality increases by 2 when there are 10 days or less', () => {
            const gildedRose = new GildedRose([new Item(ItemName.BackstagePasses, 9, 1)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(1);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(3);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(5);
        });
        it('Quality increases by 3 when there are 5 days or less', () => {
            const gildedRose = new GildedRose([new Item(ItemName.BackstagePasses, 5, 1)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(1);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(4);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(7);
        });
        it('Quality drops to 0 after the concert', () => {
            const gildedRose = new GildedRose([new Item(ItemName.BackstagePasses, 0, 10)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(10);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(0);
        });
        it('Quality never exceeds 50', () => {
            const gildedRose = new GildedRose([new Item(ItemName.BackstagePasses, 10, 50)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(50);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(50);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(50);
        });
    });
    describe(ItemName.Conjured, () => {
        it('Degrades in Quality twice as fast as normal items', () => {
            const gildedRose = new GildedRose([new Item(ItemName.Conjured, 10, 10)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(10);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(8);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(6);
        });
        it('Doesn’t go below 0 quality', () => {
            const gildedRose = new GildedRose([new Item(ItemName.Conjured, 10, 0)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(0);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(0);
        });
        it('Degrades in Quality twice as fast as normal items when expired', () => {
            const gildedRose = new GildedRose([new Item(ItemName.Conjured, 0, 10)]);
            const item = gildedRose.items[0];
            expect(item.quality).to.equal(10);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(6);
            gildedRose.updateQuality();
            expect(item.quality).to.equal(2);
        });
    });

});
