export enum ItemName {
    AgedBrie = 'Aged Brie',
    Sulfuras = 'Sulfuras, Hand of Ragnaros',
    BackstagePasses = 'Backstage passes to a TAFKAL80ETC concert',
    Conjured = 'Conjured Mana Cake'
};

const DEGRADATION_RATE = {
    [ItemName.AgedBrie]: -1,
    [ItemName.Conjured]: 2,
    [ItemName.Sulfuras]: 0,
    default: 1
};

export class Item {
    name: string;
    sellIn: number;
    quality: number;

    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

class QualityProcess {

    private static processQualityForBackstagePass(quality: number, sellIn: number) {
        let processedQuality = quality + 1;
        if (sellIn < 10) {
            processedQuality++;
        }
        if (sellIn < 5) {
            processedQuality++;
        }
        return processedQuality;
    }

    private static processQualityForExpiredItems(name: string, quality: number) {
        if (name === ItemName.BackstagePasses) {
            return 0;
        }
        return quality - this.getDegradationRate(name);
    }

    private static normaliseQuality(name: string, quality: number) {
        if (quality <= 0) {
            return 0;
        } else if (name !== ItemName.Sulfuras && quality > 50) {
            return 50;
        }
        return quality;
    }

    private static getDegradationRate(name: string) {
        return DEGRADATION_RATE.hasOwnProperty(name) ? DEGRADATION_RATE[name] : DEGRADATION_RATE.default;
    }

    static getUpdatedSellIn(name: string, sellIn: number) {
        if (name === ItemName.Sulfuras) {
            return sellIn;
        }
        return sellIn - 1;
    }

    static getProcessedQuality(name: string, quality: number, sellIn: number) {
        let processedQuality;
        if (name === ItemName.BackstagePasses) {
            processedQuality = this.processQualityForBackstagePass(quality, sellIn);
        } else {
            processedQuality = quality - this.getDegradationRate(name);
        }
        if (sellIn < 0) {
            processedQuality = this.processQualityForExpiredItems(name, processedQuality);
        }
        return this.normaliseQuality(name, processedQuality);
    }

}

export class GildedRose {
    items: Array<Item>;

    constructor(items = [] as Array<Item>) {
        this.items = items;
    }

    updateQuality() {
        return this.items.map((item) => {
            const { name, quality, sellIn, } = item;
            const updatedSellIn = QualityProcess.getUpdatedSellIn(name, sellIn);
            item.sellIn = updatedSellIn;
            item.quality = QualityProcess.getProcessedQuality(name, quality, updatedSellIn);
            return item;
        });
    }
}
