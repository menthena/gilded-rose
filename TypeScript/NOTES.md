# Analysis & Plan

After analysing the code, I decided it is best to start with writing unit tests
for the existing scenarios. Which will enable me to add features without
breaking functionality.

The method `updateQuality` is the problem here, which needs improving.
It is doing all the hardwork, difficult to understand and it is 
not open for extension as the number of lines will increase as the requirements
grow. Need to break into pieces.
